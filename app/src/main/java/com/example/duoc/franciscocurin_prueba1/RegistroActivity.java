package com.example.duoc.franciscocurin_prueba1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.duoc.franciscocurin_prueba1.BaseDatos.BaseDeDatos;
import com.example.duoc.franciscocurin_prueba1.Entidades.Usuario;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText etUsuario,etClave, etRClave;
    private Button btnCrear, btnVolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        etUsuario=(EditText) findViewById(R.id.etUsuario);
        etClave=(EditText) findViewById(R.id.etClave);
        etRClave=(EditText) findViewById(R.id.etRClave);

        btnCrear=(Button) findViewById(R.id.btnCrear);
        btnVolver=(Button) findViewById(R.id.btnVolver);

        btnCrear.setOnClickListener(this);
        btnVolver.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btnCrear) {
            String mensajeError = "";

            if (etUsuario.getText().toString().length() <= 0) {
                mensajeError += "Usuario esta vacio \n";

            }else
            if (etClave.getText().toString().length() <= 0) {
                mensajeError += "Clave esta vacia \n";
            }else
            if (etRClave.getText().toString().length() <= 0) {
                mensajeError += "Repetir Clave esta vacia \n";
            }else
            if (!etRClave.getText().toString().equals(etClave.getText().toString())) {
                mensajeError += "Claves No Coinciden \n";
            } else {
                Usuario user = new Usuario();
                user.setNombre(etUsuario.getText().toString());
                user.setClave(etRClave.getText().toString());
                BaseDeDatos.agregarUsuario(user);
                Toast.makeText(this, "Usuario Registrado", Toast.LENGTH_SHORT).show();

                /*
                Limpiado
                 */
                etRClave.setText("");
                etUsuario.setText("");
                etClave.setText("");

            }
            Toast.makeText(this, mensajeError, Toast.LENGTH_SHORT).show();

        }
        else if(v.getId()==R.id.btnVolver){
            this.finish();
        }


    }
}
