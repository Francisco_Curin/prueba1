package com.example.duoc.franciscocurin_prueba1.Entidades;

/**
 * Created by DUOC on 20-04-2017.
 */

public class Usuario {
    private String nombre;
    private String clave;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }



}
