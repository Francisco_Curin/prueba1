package com.example.duoc.franciscocurin_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.duoc.franciscocurin_prueba1.BaseDatos.BaseDeDatos;
import com.example.duoc.franciscocurin_prueba1.Entidades.Usuario;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText txUsuario, txClave;
    private Button btnRegistro, btnEntrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txUsuario =(EditText) findViewById(R.id.txUsuario);
        txClave =(EditText) findViewById(R.id.txClave);

        btnEntrar=(Button) findViewById(R.id.btnEntrar);
        btnRegistro=(Button) findViewById(R.id.btnRegistro);

        btnEntrar.setOnClickListener(this);
        btnRegistro.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if(v.getId()== R.id.btnRegistro)
        {
            Intent i= new Intent(LoginActivity.this, RegistroActivity.class);
            startActivity(i);

        }
        else if(v.getId()==R.id.btnEntrar){
            String mensajeError = "";
            if(txUsuario.getText().toString().length()<=0) {
                mensajeError += "Usuario vacio \n";
            }else
            if (txClave.getText().toString().length()<=0) {

                mensajeError += "Clave vacia \n";
            }
            else{
                for(Usuario user:BaseDeDatos.obtieneListadoUsuarios()) {
                    if (txUsuario.getText().toString().equals(user.getNombre()) && txClave.getText().toString().equals(user.getClave())) {
                        Intent i = new Intent(LoginActivity.this, ListadoUsuario.class);
                        startActivity(i);
                    }
                }
            }
            Toast.makeText(this, mensajeError,Toast.LENGTH_SHORT ).show();
        }


    }
}
