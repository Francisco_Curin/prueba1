package com.example.duoc.franciscocurin_prueba1.BaseDatos;

import com.example.duoc.franciscocurin_prueba1.Entidades.Usuario;

import java.util.ArrayList;

/**
 * Created by DUOC on 20-04-2017.
 */

public class BaseDeDatos extends ArrayList{
    private static ArrayList<Usuario> values = new ArrayList<>();

    public static void agregarUsuario(Usuario usuario){
        values.add(usuario);
    }

    public static ArrayList<Usuario> obtieneListadoUsuarios(){
        return values;
    }
}
